N = [200, 300, 400, 500, 600, 700]
D = [1, 15, 30, 60]
P = [100, 1000]
A = [0, 1, 2]
PE = [0, 1]
T = 600

import os


for a in A:
    for pe in PE:
        for n in N:
            for d in D:
                for p in P:
                    inst = 'R.{}.{}.{}'.format(n,p,d)
                    print("########## A: {}\t PE: {}\t inst: {}\t ##########".format(a,pe,inst))
                    os.system("./runExperiment.exe insts/{}.sop {} sols_mst/{}_{}_{}.sol {} {}".format(inst, T, a, pe, inst, a, pe))