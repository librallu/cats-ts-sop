#pragma once

#include <vector>
#include <cassert>
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <omp.h>

#include "../../cats-framework/include/util/SubsetInt.hpp"
#include "../../cats-framework/include/combinators/PrefixEquivalenceCombinator.hpp"

using namespace cats;

/**
 * SOP prefixEquivalence
 */
struct PrefixEquivalenceSOP {
    SubsetInt subset;
    int last;

    PrefixEquivalenceSOP(const PrefixEquivalenceSOP& n) :
        subset(n.subset), last(n.last) {}
    PrefixEquivalenceSOP(const SubsetInt sub, int l) : subset(sub), last(l) {}

    bool operator==(const PrefixEquivalenceSOP& a) const {
        if ( last != a.last ) return false;
        return subset == a.subset;
    }

    friend std::ostream &operator<<(std::ostream& out, const PrefixEquivalenceSOP& n) {
        // out << n.subset << "#" << n.last;
        out << n.last;
        return out;
    }
};

/**
 * hash function taking a nodeEquivalenceSOP as a parameter
 */
struct nodeEqHash {
    size_t operator()(const PrefixEquivalenceSOP& n) const noexcept {
        size_t seed = n.subset.hash();
        boost::hash_combine(seed, static_cast<size_t>(n.last));
        return seed;
    }
};
