#include <iostream>
#include <set>
#include <omp.h>
#include <signal.h>
#include <fstream>

#include "../../cats-framework/include/util/includeAlgorithms.hpp"
#include "../../cats-framework/include/SearchManager.hpp"
#include "../../cats-framework/include/combinators/PrefixEquivalenceCombinator.hpp"

#include "InstanceSOP.hpp"
#include "BSSOP1.hpp"
#include "BSSOP2.hpp"
#include "BSSOP3.hpp"
#include "StoreSOP.hpp"

using namespace cats;

/**
 * global objects used by the end search handler
 */
SearchManager search_manager = SearchManager();
PrefixEquivalenceManager prefix_equivalence_manager;
GenericPEStoreHash<PrefixEquivalenceSOP, nodeEqHash> prefix_equivalence_store;

/**
 * print statistics at the end of the search
 */
void handle_end_search() {
    std::cout << "=== SEARCH ENDED ===" << std::endl;
    search_manager.printStats();
    prefix_equivalence_manager.printStats();
    prefix_equivalence_store.printStats();
    search_manager.writeJSONFile();
    exit(1);
}

enum SearchStrategy {
    Dfs,
    Beamsearch,
    Lds,
    Bg
};


int main(int argc, char* argv[]) {
    if ( argc < 6 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE_NAME TIME_LIMIT OUTPUT_FILENAME SEARCH PREFIX_EQUIV" << std::endl;
        std::cout << "SEARCH:\n\t0: DFS\n\t1: Beam Search\n\t2: LDS\n\t3: Branch and Greed\n";
        std::cout << "PREFIX_EQUIV:\n\t0: NO\n\t1: YES\n";
        return 1;
    }

    // parse user input
    InstanceSOP inst(argv[1]);
    std::cout << "###\t" << argv[1] << std::endl;
    int time_limit = std::stoi(argv[2]);
    std::string output_filename = argv[3];
    SearchStrategy search_strategy = static_cast<SearchStrategy>(std::stoi(argv[4]));
    bool prefix_equiv = std::stoi(argv[5]);


    /** create event handlers */
    signal(SIGINT, [](int signal) {
        fprintf(stderr, "Error user kill: signal %d:\n", signal);
        handle_end_search();
        exit(1);
    });


    // define root of the tree
    // BranchingPtr root_ptr = BranchingPtr(new BSSOP1(inst, output_filename));
    // BranchingPtr root_ptr = BranchingPtr(new BSSOP2(inst, output_filename));
    BranchingPtr root_ptr = BranchingPtr(new BSSOP3(inst, output_filename));

    if ( prefix_equiv ) {
        root_ptr = BranchingPtr(new PrefixEquivalenceCombinator<PrefixEquivalenceSOP>(root_ptr, prefix_equivalence_store, prefix_equivalence_manager));
    }


    // start search manager
    search_manager.start();

    // define tree search
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    if ( search_strategy == Dfs ) {
        DFS ts = DFS(ts_params);
        ts.run(time_limit);
    } else if ( search_strategy == Beamsearch ) {
        IterativeBeamSearch ts = IterativeBeamSearch(ts_params, 1, 2., true);
        ts.run(time_limit);
    } else if ( search_strategy == Lds ) {
        LDS ts = LDS(ts_params);
        ts.run(time_limit);
    } else {
        BranchAndGreed ts = BranchAndGreed(ts_params, 1.);
        ts.run(time_limit);
    }

    std::ofstream bounds_file;
    bounds_file.open(output_filename+".bounds");
    bounds_file << search_manager.getDual() << "," << search_manager.getBest().get()->evaluate() << std::endl;
    bounds_file.close();

    handle_end_search();
    return 0;
}
