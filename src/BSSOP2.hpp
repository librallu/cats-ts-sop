#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>
#include <set>


#include "../../cats-framework/include/BranchingScheme.hpp"

#include "InstanceSOP.hpp"
#include "StoreSOP.hpp"
#include "checker.hpp"

using namespace cats;

struct Arc {
    NodeId u;
    NodeId v;
};

struct CompareArc {
    InstanceSOP& inst_;

    explicit CompareArc(InstanceSOP& inst) : inst_(inst) {}

    bool operator()(const Arc a, const Arc b) {
        return inst_.get_weight_undirected(a.u, a.v) < inst_.get_weight_undirected(b.u, b.v);
    }
};


class BSSOP2 : public PrefixEquivalenceBranchingScheme<PrefixEquivalenceSOP> {
 private:
    InstanceSOP& inst_;   ///< reference to the instance
    std::vector<NodeId> prefix_;  ///< partial permutation of node
    Weight cost_prefix_;  ///< sum of edges already selected
    Weight cost_suffix_;  ///< value of directed MST
    SubsetInt added_subset_;  ///< set containing added vertices so far (used to compute quickly prefix equivalences)
    std::string& output_filename_;  ///< reference to the outpuf file to be writen when finding new improving solution

 public:
    explicit BSSOP2(InstanceSOP& inst, std::string& output_filename): BranchingScheme(),
        inst_(inst), cost_prefix_(0), cost_suffix_(0), added_subset_(), output_filename_(output_filename) {
            prefix_.push_back(inst_.getStartVertex());
            computeDirectedMst();
        }

    explicit BSSOP2(const BSSOP2& s): BranchingScheme(s),
        inst_(s.inst_), prefix_(s.prefix_), cost_prefix_(s.cost_prefix_), cost_suffix_(s.cost_suffix_),
        added_subset_(s.added_subset_), output_filename_(s.output_filename_) {
            // std::cout << to_string() << std::endl;
        }

    BranchingPtr copy() const override { return BranchingPtr(new BSSOP2(*this)); }

    double evalPrefix() const override {
        return cost_prefix_;
    }

    double evalSuffix() const override {
        return cost_suffix_;
    }

    double guide() override {
        return evaluate();
    }

    std::vector<BranchingPtr> getChildren(double& smallest_heuristic_cut) override {
        std::vector<BranchingPtr> res;
        NodeId last_city_ = prefix_[prefix_.size()-1];
        for ( NodeId neigh : inst_.get_neighbours_by_rank(last_city_) ) {
            // for each vertex, check if we can add it
            bool to_add = neigh != last_city_ && !added_subset_.contains(neigh);  // check that it is not already added
            if ( to_add ) {
                for ( NodeId pred : inst_.precedences_of(neigh) ) {  // if not, check all precedences of neigh
                    if ( pred != last_city_ && !added_subset_.contains(pred) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                BSSOP2* child = new BSSOP2(*this);
                child->add_city(neigh);
                res.push_back(BranchingPtr(child));
            }
        }
        return res;
    }

    inline bool isGoal() const override { return inst_.get_nb_vertices() == static_cast<int>(prefix_.size()); }

    /**
     * \brief debug only. Display content of node
     */
    std::string to_string() {
        std::string res = "prefix(";
        for ( auto e : prefix_ ) {
            res += std::to_string(e)+" ";
        }
        return res;
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest(SearchManager& manager) override {
        // double check_val = checker(inst_, prefix_);
        // std::cout << "eval checker: " << check_val << std::endl;
        assert(checker(inst_, prefix_) == this->evaluate());
        // write solution file
        std::ofstream f;
        f.open(output_filename_);
        for ( NodeId p : prefix_ ) {
            f << p << " ";
        }
        f << "\n";
        f.close();
    }

    PrefixEquivalenceSOP getPrefixEquivalence() const override {
        return PrefixEquivalenceSOP(added_subset_, getLastCity());
    }

    std::string getName() override {
        return "SOP2";
    }

 private:
    /**
     * \brief adds a given city to the current node
     */
    void add_city(NodeId j) {
        NodeId i = getLastCity();
        added_subset_.add(i);
        prefix_.push_back(j);
        // update bounds by calling directed MST computation
        cost_prefix_ += inst_.get_weight(i, j);
        computeDirectedMst();
    }

    NodeId getLastCity() const {
        return prefix_[prefix_.size()-1];
    }

    /**
     * \brief computes a directed MST suffix lower bound
     */
    void computeDirectedMst() {
        NodeId s = getLastCity();
        // std::cout << "==============\n";
        // std::cout << "[";
        // for ( auto e : prefix_ ) {
        //     std::cout << e << " ";
        // }
        // std::cout << "]\n";
        // define data structures
        SubsetInt added_mst;
        added_mst.add(s);
        cost_suffix_ = 0;
        NodeId nb_added = 0;
        std::multiset<Arc, CompareArc> out_arcs = std::multiset<Arc, CompareArc>(CompareArc(inst_));
        for ( NodeId e : inst_.get_neighbours_by_rank(s) ) {
            if ( !added_subset_.contains(e) ) {
                out_arcs.insert({.u = s, .v = e});
            }
        }
        while ( nb_added + static_cast<int>(prefix_.size()) != inst_.get_nb_vertices() ) {
            if ( out_arcs.empty() ) {
                cost_suffix_ = infty;
                return;
            }
            Arc arc = *out_arcs.begin();
            out_arcs.erase(out_arcs.begin());
            if ( !added_mst.contains(arc.v) && !added_subset_.contains(arc.v) ) {  // if arc points to new vertex
                added_mst.add(arc.v);
                nb_added++;
                cost_suffix_ += inst_.get_weight_undirected(arc.u, arc.v);
                // std::cout << arc.u << "," << arc.v << std::endl;
                for ( NodeId c : inst_.get_neighbours_by_rank(arc.v) ) {
                    if ( !added_mst.contains(c) && !added_subset_.contains(c) && arc.v != inst_.getDestinationVertex() ) {
                        out_arcs.insert({.u = arc.v, .v = c});
                    }
                }
            }
        }
        // std::cout << "--------------\n";
    }
};
