#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>
#include <random>

#include "../../cats-framework/include/BranchingScheme.hpp"
#include "../../cats-framework/include/numeric.hpp"

#include "InstanceSOP.hpp"
#include "StoreSOP.hpp"
#include "checker.hpp"

using namespace cats;

class BSSOP3 : public PrefixEquivalenceBranchingScheme<PrefixEquivalenceSOP> {
 private:
    InstanceSOP& inst_;
    std::vector<NodeId> prefix_;
    Weight cost_prefix_;
    SubsetInt added_subset_;
    std::string& output_filename_;

 public:
    explicit BSSOP3(InstanceSOP& inst, std::string& output_filename): BranchingScheme(),
        inst_(inst), cost_prefix_(0), added_subset_(), output_filename_(output_filename) {
            prefix_.push_back(inst_.getStartVertex());
        }

    explicit BSSOP3(const BSSOP3& s): BranchingScheme(s),
        inst_(s.inst_), prefix_(s.prefix_), cost_prefix_(s.cost_prefix_), added_subset_(s.added_subset_), output_filename_(s.output_filename_) {}

    BranchingPtr copy() const override { return BranchingPtr(new BSSOP3(*this)); }

    double evalPrefix() const override {
        return cost_prefix_;
    }

    double evalSuffix() const override {
        return 0;
    }

    double evaluate() const override {
        return evalPrefix();
    }

    std::vector<BranchingPtr> getChildren(double& smallest_heuristic_cut) override {
        std::vector<BranchingPtr> res;
        NodeId last_city_ = prefix_[prefix_.size()-1];
        for ( NodeId neigh : inst_.get_neighbours_by_rank(last_city_) ) {
            // for each vertex, check if we can add it
            bool to_add = neigh != last_city_ && !added_subset_.contains(neigh);  // check that it is not already added
            if ( to_add ) {
                for ( NodeId pred : inst_.precedences_of(neigh) ) {  // if not, check all precedences of neigh
                    if ( pred != last_city_ && !added_subset_.contains(pred) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                BSSOP3* child = new BSSOP3(*this);
                child->add_city(neigh);
                res.push_back(BranchingPtr(child));
            }
        }
        return res;
    }

    inline bool isGoal() const override {
        return inst_.get_nb_vertices() == prefix_.size();
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest(SearchManager& manager) override {
        double check_val = checker(inst_, prefix_);
        // std::cout << "eval checker: " << check_val << "\teval: " << evaluate() << std::endl;
        assert(check_val == this->evaluate());
        // write solution file
        std::ofstream f;
        f.open(output_filename_);
        for ( NodeId p : prefix_ ) {
            f << p << " ";
        }
        f << "\n";
        f.close();
    }

    PrefixEquivalenceSOP getPrefixEquivalence() const override {
        return PrefixEquivalenceSOP(added_subset_, prefix_[prefix_.size()-1]);
    }

    std::string getName() override {
        return "SOP3";
    }

 private:
    /**
     * \brief adds a given city to the current node
     */
    void add_city(NodeId j) {
        NodeId i = prefix_[prefix_.size()-1];
        added_subset_.add(i);
        prefix_.push_back(j);
        // update bounds
        Weight w = inst_.get_weight(i, j);
        cost_prefix_ += w;
    }
};
