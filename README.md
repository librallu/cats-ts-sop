# CATS-SOP

Code sources of experiments performed for "Tree search algorithms for the Sequential Ordering Problem"

The present code uses the CATS framework (Combinator-based Anytime Tree Search)


## Installation and compilation

This project requires to be positioned next to [cats-framework](https://gitlab.com/librallu/cats-framework) v0.1

 1. `mkdir ~/cats-dev ; cd ~/cats-dev`
 2. `git clone https://gitlab.com/librallu/cats-framework`
 3. `cd cats-framework/ ; git checkout tags/v0.1 ; cd ..`
 4. `git clone https://gitlab.com/librallu/cats-ts-sop` 
 5. `cd cats-ts-sop/`
 6. `cmake . ; make`


## compile and run 

This project produces 2 executables:
 - checker.exe INST_FILE SOL_FILE : checks if a solution is feasible
 - runExperiment.exe INST_FILE TIME_LIMIT SOL_FILENAME SEARCH PREFIX_EQUIVALENCE: runs a tree search algorithm

Example usage:
```
./runExperiment.exe insts/R.700.1000.15.sop 600 out.sol 1 1
```